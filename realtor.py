# coding=utf-8
import requests
import json
import pdb
import sys, os
from bs4 import BeautifulSoup as Soup
import time
import urllib.parse
import random

uri_rlt = 'http://realtor.com'
uri_loc = uri_rlt + '/validate_geo?location=%s'
uri_filter = '/beds-3/baths-1/price-na-75000/sqft-750/age-0+75'

UAS = [
    "Links (2.1pre37; Linux 2.6.26-2-686 i686; 182x78)",
    "Links (2.2; OpenBSD 4.8 i386; x)",
    "Links (2.2; NetBSD 5.0 i386; 80x25)",
    "Links (2.2; Linux 3.6.9-1.el6.elrepo.x86_64 x86_64; 168x44)",
    "Links (2.2; Linux 2.6.32-gentoo-r6 x86_64; 129x42)",
    "Links (2.2; Linux 2.6.32-fw3 i686; 143x53)",
    "Links (2.2; Linux 2.6.30-ARCH x86_64; 160x50)",
    "Links (2.2; Linux 2.6.28-gentoo-r5 i686; x)",
    "Links (2.2; Linux 2.6.28-14-generic i686; 80x24)",
    "Links (2.2; Linux 2.6.28-11-server i686; 80x24)",
    "Links (2.2; Linux 2.6.27-hardened-r7 x86_64; x)",
    "Links (2.2; Linux 2.6.25-gentoo-r9 sparc64; 166x52)",
    "Links (2.2; Linux 2.6.24.4-desktop586-3mnb i686; x)",
    "Links (2.2; Linux 2.6.24.4-desktop586-3mnb i686; 141x19)",
    "Links (2.2; FreeBSD 8.1-RELEASE i386; 196x84)"
]

s = requests.Session()

houses = {}
if os.path.exists("houses.json"):
    with open('houses.json', 'r+') as dump:
        houses = json.loads(dump.read())

def save_houses(city, hs):
    houses[city.strip()] = hs
    with open('houses.json', 'w') as dump:
        dump.write(json.dumps(houses, indent=2))

def get_houses(city):
    city = city.strip()
    print('Get houses', city)

    user_agent = random.choice(UAS) # + str(int(random.random() * 1000))
    s.headers['User-Agent'] = user_agent
    # s.headers['Referer'] = uri_rlt
    try:
        loc = s.get(uri_loc % urllib.parse.quote(city))
    except Exception as e:
        print("Realtor error", e)
        return houses.get(city, {})

    loc = json.loads(loc.text)
    print(uri_rlt + loc['url'] + uri_filter)
    try:
        realtor = s.get(uri_rlt + loc['url'] + uri_filter)
    except Exception as e:
        print("Realtor error", e)
        return houses.get(city, {})

    with open('dump.html', 'w') as out:
        out.write(realtor.text)
    return parse(city, realtor.text)
    

def get_value(el, tag, params):
    res = el.find(tag, params)
    return res and res.text.strip() or res.attributes.get('content', '')

def parse(place, text):
    soup = Soup(text, "html.parser")
    pdb.set_trace()

    els = soup.findAll('li', {"class": 'component_property-card'})
    hs = dict()
    for el in els:
        url = el.attrs.get('data-url')
        pid = el.attrs.get('data-propertyid')
        if pid in houses[place].keys():
            continue

        lid = el.attrs.get('data-listingid')
        if not url:
            continue

        city    = get_value(el, 'span', {'itemprop': "addressLocality"})
        address = get_value(el, 'span', {'itemprop': "streetAddress"})
        pcode   = get_value(el, 'span', {'itemprop': "postalCode"})
        price   = get_value(el, 'span', {'class': "data-price"})
        lat     = get_value(el, 'meta', {'itemprop': "latitude"})
        lon     = get_value(el, 'meta', {'itemprop': "longitude"})
        lon     = get_value(el, 'meta', {'itemprop': "dateModified"})

        hs[pid] = dict(
            url = url,
            place = place,
            sent = False,
            pid = pid,
            lid = lid,
            city = city,
            address = address,
            pcode = pcode,
            price = price,
            lat = lat,
            lon = lon,
        )

    return hs

