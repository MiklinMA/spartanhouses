# coding=utf-8
import requests
import json
import pdb
import sys, os
from bs4 import BeautifulSoup as Soup
import time
import urllib.parse
import zillow
import crime
import realtor
from datetime import datetime

with open("zillow_key.conf", 'r') as f:
    zkey = f.readline().replace("\n", "")

zapi = zillow.ValuationApi()

user_agent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36"

s = requests.Session()
s.headers['User-Agent'] = user_agent

def gov_search(place, houses):
    uri_gov = 'https://accessmygov.com'
    for house in houses.values():
        print()
        print(house['pcode'], house['address'])

        c = crime.get_crimes_count(house['pcode'], house['address'])
        print(' Crimes', c)
        house['crimes'] = c

        print(' Zillow')
        if not house.get('zamount'):

            for i in range(3):
                address = house['address']
                if i:
                    address = " ".join(address.split()[:-i])
                try:
                    data = zapi.GetSearchResults(zkey, house['address'], house['pcode'])
                    house['zamount'] = data.zestimate.amount
                    break
                except Exception as e:
                    print('  error', address)

        print('  amount:', house.get('zamount', 'NOT FOUND'))

        print(' Accessmygov')
        with s.get(uri_gov + '/MunicipalDirectory/UnitDirectoryGrid?searchValue=%s&stateSearchValue=MI' %
                urllib.parse.quote(house['city'])
                ) as mun:
            soup = Soup(mun.text, "html.parser")
        ms = soup.findAll('tr', {"class": 'unit-directory-row'})

        for m in ms:
            uid = m.td.get('id')
            s.get(uri_gov + '/MunicipalDirectory/SelectUnit?unitId=%s&returnUrl=' % uid)

            for i in range(3):
                address = house['address']
                if i:
                    address = " ".join(address.split()[:-i])

                r = s.get('https://accessmygov.com/SiteSearch/SiteSearchResults?SearchFocus=All+Records&SearchCategory=Address&SearchText=%s&AddrSearchStreetName=&AddrSearchStreetNumFrom=&AddrSearchStreetNumTo=&UseAdvancedAddrSearch=false' %
                    urllib.parse.quote(address)
                )
                rs = Soup(r.text, "html.parser")
                k = rs.find('tr', {"class": 'site-search-row'})
                if k:
                    print(' ', m.td.div.text, address, 'OK')
                    break
                else:
                    print(' ', m.td.div.text, address, 'failed')
            else:
                print(' ', m.td.div.text, 'NOT FOUND')
                time.sleep(1)
                continue

            gurl = k.find('td').text

            gurl = gurl.replace(';=', '=')
            gurl = gurl.replace('+', '%20')
            gurl = gurl.replace('6%3d', '')
            gurl = gurl.replace(':', '')

            u = urllib.parse.urlparse(gurl)
            ps = urllib.parse.parse_qsl(u.query)
            ps = dict(ps)
            for k, v in ps.items():
                j = 0
                for c in v:
                    if j and c == '=':
                        # pdb.set_trace()
                        ps[k] = v[:j]
                        break
                    j += 1
            rks = ps.get('RecordKeyDisplayString')
            if rks and len(rks) == 16:
                ps['RecordKey'] = rks

            ps['RecordKeyType'] = 0

            # print(json.dumps(ps, indent=2))
            gurl = urllib.parse.urlencode(ps)
            gurl = "%s?%s" % (u.path, gurl)
            gurl = gurl.replace('+', '%20')

            gurl_orig = gurl
            s.get(uri_gov + gurl)

            gurl = gurl.replace('SiteSearch/SiteSearchDetails', 'ASSG_SiteSearch/LoadContent')
            print('   Sales')
            r = s.get(uri_gov + gurl)
            rs = Soup(r.text, "html.parser")
            tab = rs.find('div', {"id": "SaleHistory"})
            if tab:
                found = False
                for row in tab.findAll('tr'):
                    tds = row.findAll('td')
                    if len(tds) > 1:
                        print('   ', tds[0].text, tds[1].text)
                        house['sale_date'] = tds[0].text
                        house['sale_price'] = tds[0].text
                        found = True
                        break
                if not found:
                    print('   ', 'No sales history found.')
            else:
                if 'AccessMyGov' not in rs.text[:150]:
                    print(rs.text[:100].strip())

            taxes_found = False

            gurl = gurl.replace('ASSG_SiteSearch/LoadContent', 'Tax_SiteSearch/LoadTaxContent')
            print('   Taxes')
            r = s.get(uri_gov + gurl)
            rs = Soup(r.text, "html.parser")
            tab = rs.find('div', {"name": "TaxHistory"})
            if tab:
                house['tax_amount'] = 0
                for row in tab.findAll('tr', {'name': 'TaxHistory'})[:10]:
                    tds = row.findAll('td')
                    print('   ', tds[1].text, tds[3].text)
                    if int(tds[1].text) > datetime.now().year - 2:
                        house['tax_amount'] += float(tds[3].text.replace('$', ''))
                    taxes_found = True
            else:
                if 'AccessMyGov' not in rs.text[:150]:
                    print(rs.text[:100].strip())

            if not taxes_found:
                gurl = gurl.replace('LoadTaxContent', 'LoadDlqPPContent')
                r = s.get(uri_gov + gurl)
                rs = Soup(r.text, "html.parser")
                tab = rs.find('div', {"name": "TaxHistory"})
                if tab:
                    house['tax_amount'] = 0
                    for row in tab.findAll('tr', {'name': 'TaxHistory'})[:10]:
                        tds = row.findAll('td')
                        print('   ', tds[1].text, tds[3].text)
                        if int(tds[1].text) > datetime.now().year - 2:
                            house['tax_amount'] += float(tds[3].text.replace('$', ''))
                        tax_res = True
                else:
                    if 'AccessMyGov' not in rs.text[:150]:
                        print(rs.text[:100].strip())

            print(uri_gov + gurl_orig)

        realtor.save_houses(place, houses)

    return houses

with open('city.txt', 'r') as cities:
    for city in cities:
        houses = realtor.get_houses(city)
        houses = gov_search(city, houses)
        realtor.save_houses(city, houses)
        time.sleep(10)

