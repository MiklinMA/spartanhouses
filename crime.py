# coding=utf-8
import requests
import json
import pdb
import ast
import os

def get_crimes_count(pcode, address):
    url = 'https://www.crimemapping.com/map/agency/197'

    s = requests.Session()
    s.get(url)

    url = 'https://www.crimemapping.com/map/GeocodeLocation'

    form = dict()
    form['id'] = ''
    form['iscollection'] = False
    form['maxlocations'] = 10
    form['location'] = "%s %s" % (pcode, address)


    res = s.post(url, data=form)
    res = json.loads(ast.literal_eval(res.text))
# print(json.dumps(res, indent=2))

    url = 'https://www.crimemapping.com/map/MapUpdated'

    fil = dict()
    fil['x'] = res['locations'][0]['feature']['attributes']['X']
    fil['y'] = res['locations'][0]['feature']['attributes']['Y']
    fil['ad'] = res['locations'][0]['feature']['attributes']['StAddr']
    fil['bd'] = "3218.69"

    fdata = dict()
    fdata['SelectedCategories'] = []
    for i in range(1, 16):
        fdata['SelectedCategories'].append(str(i))
    fdata['SpatialFilter'] = {
        "FilterType": 0,
        "Filter": json.dumps(fil)
      }
    fdata['TemporalFilter'] = {
        "PreviousID": "4",
        "PreviousNumDays": 28,
        "PreviousName": "Previous 4 Weeks",
        "FilterType": "Previous",
        "ExplicitStartDate": "20180316",
        "ExplicitEndDate": "20180412"
      }
    fdata['AgencyFilter'] = []

    sdata = dict()
    sdata['rings'] = [
        [
#         [-9534217.261239652,5291558.448622952],
#         [-9534217.261239652,5304323.432346559],
#         [-9514821.365312316,5304323.432346559],
#         [-9514821.365312316,5291558.448622952],
#         [-9534217.261239652,5291558.448622952]
        ]
    ]
    sdata["spatialReference"] = dict()
    sdata["spatialReference"]["wkid"] = res['spatialReference']['wkid']
    sdata["spatialReference"]["latestWkid"] = res['spatialReference']['latestWkid']

    form = dict()
    form['shareMapID'] = ""
    form['shareMapExtent'] = ""
    form['alertID'] = ""
    form['filterdata'] = json.dumps(fdata)
    form['spatfilter'] = json.dumps(sdata)

    s = requests.Session()
    res = s.post(url, data=form)
# print(res.request.body)
# print(res.request.headers)
    try:
        res = json.loads(ast.literal_eval(res.text))
    except:
        res = res.text
    res = json.loads(res)
    crimes = res['result']['rs']
    count = 0
    for crime in crimes:
        # print(crime['i'])
        count += len(crime['i'])
    return count


# houses = {}
# if os.path.exists("houses.json"):
#     with open('houses.json', 'r+') as dump:
#         houses = json.loads(dump.read())
# 
# for house in houses.values():
#     c = get_crimes_count(house['pcode'], house['address'])
#     print(house['pcode'], house['address'], c)


